/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author jose
 */
public class ConsultaMedicamento extends Conexion {

    private final ArrayList<String> nombre = new ArrayList<>();
    private final ArrayList<String> codigo = new ArrayList<>();
    private final ArrayList<Integer> cantidadmg = new ArrayList<>();
    private final ArrayList<String> laboratorio = new ArrayList<>();
    private final ArrayList<String> componentes = new ArrayList<>();
    private final ArrayList<Integer> tipo = new ArrayList<>();
    private final ArrayList<String> generico = new ArrayList<>();
    private final ArrayList<String> presentacion = new ArrayList<>();

    private String nombre_Actual;
    private String codigo_Actual;
    private int cantidadmg_Actual;
    private String laboratorio_Actual;
    private int tipo_Actual;
    private String generico_Actual;
    private String presentacion_Actual;

    public ConsultaMedicamento() {
    }

    public void ActualizarArray() {
        nombre.clear();
        codigo.clear();
        cantidadmg.clear();
        laboratorio.clear();
        componentes.clear();
        tipo.clear();
        generico.clear();
        presentacion.clear();

        Connection con = getConexion();

        sql = "select * from medicamento;";

        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            codigo.add(rs.getString(1));
            nombre.add(rs.getString(2));
            cantidadmg.add(rs.getInt(3));
            tipo.add(rs.getInt(4));
            generico.add(rs.getString(5));
            presentacion.add(rs.getString(6));
            while (rs.next()) {

            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public String getNombre_Actual() {
        return nombre_Actual;
    }

    public void setNombre_Actual(String nombre_Actual) {
        this.nombre_Actual = nombre_Actual;
    }

    public String getCodigo_Actual() {
        return codigo_Actual;
    }

    public void setCodigo_Actual(String codigo_Actual) {
        this.codigo_Actual = codigo_Actual;
    }

    public int getCantidadmg_Actual() {
        return cantidadmg_Actual;
    }

    public void setCantidadmg_Actual(int cantidadmg_Actual) {
        this.cantidadmg_Actual = cantidadmg_Actual;
    }

    public String getLaboratorio_Actual() {
        return laboratorio_Actual;
    }

    public void setLaboratorio_Actual(String laboratorio_Actual) {
        this.laboratorio_Actual = laboratorio_Actual;
    }

    public int getTipo_Actual() {
        return tipo_Actual;
    }

    public void setTipo_Actual(int tipo_Actual) {
        this.tipo_Actual = tipo_Actual;
    }

    public String getGenerico_Actual() {
        return generico_Actual;
    }

    public void setGenerico_Actual(String generico_Actual) {
        this.generico_Actual = generico_Actual;
    }

    public String getPresentacion_Actual() {
        return presentacion_Actual;
    }

    public void setPresentacion_Actual(String presentacion_Actual) {
        this.presentacion_Actual = presentacion_Actual;
    }

    public ArrayList<String> getNombre() {
        return nombre;
    }

    public ArrayList<String> getCodigo() {
        return codigo;
    }

    public ArrayList<Integer> getCantidadmg() {
        return cantidadmg;
    }

    public ArrayList<String> getLaboratorio() {
        return laboratorio;
    }

    public ArrayList<String> getComponentes() {
        return componentes;
    }

    public ArrayList<Integer> getTipo() {
        return tipo;
    }

    public ArrayList<String> getGenerico() {
        return generico;
    }

    public ArrayList<String> getPresentacion() {
        return presentacion;
    }

}
