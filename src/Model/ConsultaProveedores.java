/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author jose
 */
public class ConsultaProveedores extends Conexion{
    private final ArrayList<String> nombre = new ArrayList<>();
    private final ArrayList<String> logo = new ArrayList<>();
    private final ArrayList<String> ruc = new ArrayList<>();
    private final ArrayList<Integer> estado = new ArrayList<>();
    
    private String nombreActual;
    private String logoActual;
    private String rucActual;
    private int estadoActual;

    public ConsultaProveedores() {
        ActualizarArray();
    }
    
    public void ActualizarArray(){
        nombre.clear();
        logo.clear();
        ruc.clear();
        estado.clear();

        Connection con = getConexion();

        sql = "select * from proveedor where estado=1;";

        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                nombre.add(rs.getString(1));
                logo.add(rs.getString(2));
                ruc.add(rs.getString(3));
                estado.add(rs.getInt(4));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    public void create(String nombreP, String logo, String ruc, int estado) {
        Connection con = getConexion();

        sql = "insert into proveedor (nombreproveedor, logoproveedor, rucproveedor,estado) VALUES (?,?,?,?);";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, nombreP);
            ps.setString(2, logo);
            ps.setString(3, ruc);
            ps.setInt(4, estado);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Registrado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al registrar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }

    public void update(String nombreP_back, String nombre, String logo, String ruc, int estado) {
        Connection con = getConexion();

        sql = "update proveedor set nombreproveedor=?, rucproveedor=?, logoproveedor=?, estado=? where nombreproveedor=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, ruc);
            ps.setString(3, logo);
            ps.setInt(4, estado);
            ps.setString(5, nombreP_back);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al actualizar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }

    public void delete(String nombreP) {
        Connection con = getConexion();

        sql = "delete proveedor where nombreproveedor=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, nombreP);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al eliminar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }
    
    public void getField(String proveedor){
        Connection con = getConexion();
        sql = "select * from proveedor where proveedor.nombreproveedor=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, proveedor);
            rs = ps.executeQuery();

            if (rs.next()) {
                nombreActual=rs.getString(1);
                logoActual=rs.getString(2);
                rucActual=rs.getString(3);
                estadoActual=rs.getInt(4);
            } else {
                JOptionPane.showMessageDialog(null, "Proveedor no encontrado");
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    public String getLogoActual() {
        return logoActual;
    }

    public void setLogoActual(String logoActual) {
        this.logoActual = logoActual;
    }

    public String getRucActual() {
        return rucActual;
    }

    public void setRucActual(String rucActual) {
        this.rucActual = rucActual;
    }

    public ArrayList<String> getNombre() {
        return nombre;
    }

    public ArrayList<String> getLogo() {
        return logo;
    }

    public ArrayList<String> getRuc() {
        return ruc;
    }

    public String getNombreActual() {
        return nombreActual;
    }

    public void setNombreActual(String nombreActual) {
        this.nombreActual = nombreActual;
    }

    public int getEstadoActual() {
        return estadoActual;
    }

    public void setEstadoActual(int estadoActual) {
        this.estadoActual = estadoActual;
    }

    public ArrayList<Integer> getEstado() {
        return estado;
    }
    
    
    
}
