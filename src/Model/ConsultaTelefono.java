/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author jose
 */
public class ConsultaTelefono extends Conexion{
    private final ArrayList<String> telefono = new ArrayList<>();
    private final ArrayList<String> tipo = new ArrayList<>();

    private String direccion;
    ConsultarSucursales cs=new ConsultarSucursales();
    
    public ConsultaTelefono(String direccion) {
        this.direccion=direccion;
        ActualizarArray();
    }

    
    
    private void ActualizarArray(){
        Connection con = getConexion();

        sql = "select *from telefono natural join sucursal where direccion=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, direccion);
            rs = ps.executeQuery();

            while (rs.next()) {
                telefono.add(rs.getString(2));
                tipo.add(rs.getString(3));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    public void create(String telefono, String tipo, String direccion) {
        cs.consultarId(direccion);
        
        Connection con = getConexion();

        sql = "insert into telefono (telefono, tipotelefono, idsucursal) values (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, telefono);
            ps.setString(2, tipo);
            ps.setString(3, cs.getIdActual());
            ps.execute();
            JOptionPane.showMessageDialog(null, "Registrado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al registrar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }

    public void read() {
        ActualizarArray();
    }

    public void update(String telefono_back,String telefono, String tipo, String direccion) {
        cs.consultarId(direccion);
        Connection con = getConexion();

        sql = "update telefono set telefono=?, tipotelefono=?, idsucursal=? where telefono=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, telefono);
            ps.setString(2, tipo);
            ps.setString(3, cs.getIdActual());
            ps.setString(4, telefono_back);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al actualizar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }

    public void delete(String telefono) {
        Connection con = getConexion();

        sql = "delete from telefono where telefono=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, telefono);
            ps.execute();
            //JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al eliminar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }
    
    public ArrayList<String> getTelefono() {
        return telefono;
    }

    public ArrayList<String> getTipo() {
        return tipo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
    
}
