/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author jose
 */
public class ConsultasUsuarios extends Conexion {

    private final ArrayList<String> usuarios = new ArrayList<>();
    private final ArrayList<String> pass = new ArrayList<>();
    private final ArrayList<String> respuesta = new ArrayList<>();
    private final ArrayList<Integer> tipoUsuario = new ArrayList<>();
    private final ArrayList<Integer> tipoPregunta = new ArrayList<>();
    private final ArrayList<String> pregunta = new ArrayList<>();

    private String usuarioActual;
    private String passActual;
    private String respuestaActual;
    private int tipoUsuarioActual;
    private int tipoPreguntaActual;

    public ConsultasUsuarios() {
        ActualizarArrays();
    }

    private void ActualizarArrays() {
        usuarios.clear();
        pass.clear();
        respuesta.clear();
        tipoUsuario.clear();
        tipoPregunta.clear();

        Connection con = getConexion();

        sql = "select * from usuario inner join pregunta where pregunta_idpregunta=idpregunta;";

        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                usuarios.add(rs.getString(1));
                pass.add(rs.getString(2));
                respuesta.add(rs.getString(3));
                tipoUsuario.add(rs.getInt(4));
                tipoPregunta.add(rs.getInt(5));
                pregunta.add(rs.getString(7));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public void create(String user, String pass, String respuesta, int tipoUsuario, int tipoPregunta) {
        Connection con = getConexion();

        sql = "insert into usuario (usuario, password, respuesta, tipouser, pregunta_idpregunta) values (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, pass);
            ps.setString(3, respuesta);
            ps.setInt(4, tipoUsuario);
            ps.setInt(5, tipoPregunta);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Registrado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al registrar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArrays();
    }

    public boolean read(String usuario, String pass) {
        Connection con = getConexion();
        sql = "select * from usuario where usuario=? and password=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, pass);
            rs = ps.executeQuery();

            return rs.next();
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public void update(String user_back, String user, String pass, String respuesta, int tipoUsuario, int tipoPregunta) {
        Connection con = getConexion();

        sql = "update usuario set  usuario=?, password=?,respuesta=?,tipouser=?,pregunta_idpregunta=? where usuario=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, pass);
            ps.setString(3, respuesta);
            ps.setInt(4, tipoUsuario);
            ps.setInt(5, tipoPregunta);
            ps.setString(6, user_back);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al actualizar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArrays();
    }

    public void delete(String user) {
        Connection con = getConexion();

        sql = "delete from usuario where usuario=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, user);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al eliminar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArrays();
    }

    public boolean getFields(String user) {
        Connection con = getConexion();
        sql = "select * from usuario where usuario=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, user);
            rs = ps.executeQuery();

            if (rs.next()) {
                usuarioActual = rs.getString(1);
                passActual = rs.getString(2);
                respuestaActual = rs.getString(3);
                tipoUsuarioActual = rs.getInt(4);
                tipoPreguntaActual = rs.getInt(5);
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Usuario no encontrado");
                return false;
            }
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public ArrayList<String> getUsuarios() {
        return usuarios;
    }

    public ArrayList<String> getPass() {
        return pass;
    }

    public ArrayList<String> getRespuesta() {
        return respuesta;
    }

    public ArrayList<Integer> getTipoUsuario() {
        return tipoUsuario;
    }

    public ArrayList<Integer> getTipoPregunta() {
        return tipoPregunta;
    }

    public String getUsuarioActual() {
        return usuarioActual;
    }

    public void setUsuarioActual(String usuarioActual) {
        this.usuarioActual = usuarioActual;
    }

    public String getPassActual() {
        return passActual;
    }

    public void setPassActual(String passActual) {
        this.passActual = passActual;
    }

    public String getRespuestaActual() {
        return respuestaActual;
    }

    public void setRespuestaActual(String respuestaActual) {
        this.respuestaActual = respuestaActual;
    }

    public int getTipoUsuarioActual() {
        return tipoUsuarioActual;
    }

    public void setTipoUsuarioActual(int tipoUsuarioActual) {
        this.tipoUsuarioActual = tipoUsuarioActual;
    }

    public int getTipoPreguntaActual() {
        return tipoPreguntaActual;
    }

    public void setTipoPreguntaActual(int tipoPreguntaActual) {
        this.tipoPreguntaActual = tipoPreguntaActual;
    }

    public ArrayList<String> getPregunta() {
        return pregunta;
    }

}
