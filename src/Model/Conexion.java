/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.*;
import java.util.logging.*;

/**
 *
 * @author jose
 */
public class Conexion {
    private String base = "FarmaciaBD";
    private String user = "root";
    private String password = "71070193";
    private String url = "jdbc:mysql://localhost:3306/" + base;
    private Connection conexion = null;
    
    protected PreparedStatement ps = null;
    protected ResultSet rs = null;
    protected String sql;
    
    public Connection getConexion()
    {
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(this.url, this.user, this.password);
            
        } catch(SQLException e)
        {
            System.err.println(e);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
      return conexion;  
    }
}
