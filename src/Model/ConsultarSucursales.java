/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author jose
 */
public class ConsultarSucursales extends Conexion{
    private final ArrayList<String> direccion = new ArrayList<>();
    private String idActual;
    
    private String nombreP;

    public ConsultarSucursales(String nombreP) {
        this.nombreP=nombreP;
        ActualizarArray();
    }

    public ConsultarSucursales() {
    }
    
    public void create(String idSucursal, String direccion, String nombreP) {
                
        Connection con = getConexion();

        sql = "insert into sucursal (idsucursal, direccion, nombreproveedor) values (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, idSucursal);
            ps.setString(2, direccion);
            ps.setString(3, nombreP);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Registrado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al registrar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }

    public void read() {
        ActualizarArray();
    }

    public void update(String id_back,String id, String nombreP, String direccion) {
        Connection con = getConexion();

        sql = "update sucursal set idsucursal=?, direccion=?,nombreproveedor=? where idsucursal=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, direccion);
            ps.setString(3, nombreP);
            ps.setString(4, id_back);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al actualizar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }

    public void delete(String direccion) {
        consultarId(direccion);
        Connection con = getConexion();

        sql = "delete from sucursal where idsucursal=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, idActual);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "Problema al eliminar");
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        ActualizarArray();
    }
    
    private void ActualizarArray(){
        Connection con = getConexion();

        sql = "select *from sucursal where nombreproveedor=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, nombreP);
            rs = ps.executeQuery();

            while (rs.next()) {
                direccion.add(rs.getString(2));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    public void consultarId(String direccion){
        Connection con = getConexion();

        sql = "select * from sucursal where direccion=?;";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, direccion);
            rs = ps.executeQuery();

            while (rs.next()) {
                idActual=rs.getString(1);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    public ArrayList<String> getDireccion() {
        return direccion;
    }

    public String getIdActual() {
        return idActual;
    }

    public void setIdActual(String idActual) {
        this.idActual = idActual;
    }
    
    
    
}
