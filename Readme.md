# Caso Farmacia

La farmacia "Sanidad" requiere un sistema de control de almacen y ventas. Para ello contamos con cierta información para el desarrollo de dicho sistema y es la siguiente:

Los proveedores son las boticas **"Lives"** y **"America"**, la botica *"Lives"* posee 2 sucursales en donde se realizan las compras, cada sucursal posee uno o más números de telefono, al recibir el pedido se deberá registrar el número de boleta, fecha, hora y comprador (con sus respectivos datos) que se está recibiendo el pedido; los medicamentos tienen diversas presentaciones (jarabes, ampollas,tabletas, etc.),su ingreso se contarán por lotes, los cuales tendremos en cuenta el nombre, cantidad de mg o ml del medicamento, laboratorio, comericial o generico y componentes y descripción. Del pedido tambien se considera el precio de costo y la cantidad, en caso de bonificacion, el precio con el que se compra el medicamento es cero. 

Se deberá alertar con un mes de anticipación a la fecha de vecimiento de los productos para liquidarlos. 

En la venta de medicamentos por defecto aprecerá el precio ingresado en la compra que se recibió, sin embargo ese precio se puede modificar en la venta sin exceder el precio de costo. La cual se almacenará para el reporte tanto diario como mensual.

Se requiere tambien almacenar las ventas del dia (medicamento, precio de costo y ganancia) para un reporte al final del mes. Cuando quedan pocos medicamentos avisar y agregar a la lista de faltos.

## Dependencias Proyecto

- [JCalendar](https://gitlab.com/JSiapo/Farmacia-BD/tree/master/Dependencias)
- [Normalizacion](https://docs.google.com/spreadsheets/d/1LV5r-z3ulcEiLaTLPaGJ-FInTd2ibHWFQg-Ipy8MhWs/edit#gid=0)

## Caracteristicas del Proyecto

Los reportes diarios y mensuales se exportarán a un archivo excel.